crumb :root do
  link "Home", root_path
end

crumb :locations do
  link 'Locations', locations_path  
end

crumb :employee_checks do |location|
  link location.get_client_and_location, location_employee_checks_path(location)
  parent :locations
end

crumb :employee_check do |location|
  link 'Employee Check'
  parent :employee_checks, location
end

crumb :edit_employee_check do |employee_check|
  link employee_check.full_name, location_employee_check_path(employee_check.location, employee_check)
  parent :employee_checks, employee_check.location
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).
