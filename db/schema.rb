# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150623195558) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "assignments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "location_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "assignments", ["location_id"], name: "index_assignments_on_location_id"
  add_index "assignments", ["user_id"], name: "index_assignments_on_user_id"

  create_table "client_steps", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "step_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "client_steps", ["client_id"], name: "index_client_steps_on_client_id"
  add_index "client_steps", ["step_id"], name: "index_client_steps_on_step_id"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.integer  "location_id"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "departments", ["location_id"], name: "index_departments_on_location_id"

  create_table "employee_checks", force: :cascade do |t|
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.integer  "gender"
    t.date     "date_of_birth"
    t.text     "basic_details_comment"
    t.integer  "ethnic_code_id"
    t.integer  "location_id"
    t.integer  "full_part_time"
    t.boolean  "smoker"
    t.boolean  "past_assessment"
    t.float    "body_fat"
    t.float    "water"
    t.float    "visceral_fat"
    t.float    "total_water"
    t.integer  "basal_metabolic_rate"
    t.integer  "systolic"
    t.integer  "diastolic"
    t.integer  "resting_hr"
    t.float    "total_cholesterol"
    t.float    "triglycerides"
    t.float    "hdl_cholesterol"
    t.float    "ldl_cholesterol"
    t.float    "td_hdl_ratio"
    t.float    "blood_sugar"
    t.integer  "test_type"
    t.string   "bmi_goal_1"
    t.string   "bmi_goal_2"
    t.string   "bmi_goal_3"
    t.integer  "bmi_motivation"
    t.string   "bp_goal_1"
    t.string   "bp_goal_2"
    t.string   "bp_goal_3"
    t.integer  "bp_motivation"
    t.string   "rhr_goal_1"
    t.string   "rhr_goal_2"
    t.string   "rhr_goal_3"
    t.integer  "rhr_motivation"
    t.integer  "metric_imperial",              default: 0
    t.float    "height_cm"
    t.integer  "height_feet"
    t.integer  "height_inches"
    t.float    "weight_kg"
    t.integer  "weight_stone"
    t.integer  "weight_lbs"
    t.string   "current_step"
    t.integer  "department_id"
    t.integer  "waist_circumference"
    t.string   "body_fat_goal_1"
    t.string   "body_fat_goal_2"
    t.string   "body_fat_goal_3"
    t.integer  "body_fat_motivation"
    t.string   "blood_pressure_goal_1"
    t.string   "blood_pressure_goal_2"
    t.string   "blood_pressure_goal_3"
    t.integer  "blood_pressure_motivation"
    t.string   "blood_sugar_goal_1"
    t.string   "blood_sugar_goal_2"
    t.string   "blood_sugar_goal_3"
    t.integer  "blood_sugar_motivation"
    t.string   "total_cholesterol_goal_1"
    t.string   "total_cholesterol_goal_2"
    t.string   "total_cholesterol_goal_3"
    t.integer  "total_cholesterol_motivation"
  end

  add_index "employee_checks", ["department_id"], name: "index_employee_checks_on_department_id"
  add_index "employee_checks", ["user_id"], name: "index_employee_checks_on_user_id"

  create_table "ethnic_codes", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fact_sheet_categories", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fact_sheet_requests", force: :cascade do |t|
    t.integer  "fact_sheet_id"
    t.integer  "employee_check_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "fact_sheet_requests", ["employee_check_id"], name: "index_fact_sheet_requests_on_employee_check_id"
  add_index "fact_sheet_requests", ["fact_sheet_id"], name: "index_fact_sheet_requests_on_fact_sheet_id"

  create_table "fact_sheets", force: :cascade do |t|
    t.string   "title"
    t.string   "pdf_file"
    t.integer  "fact_sheet_category_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "fact_sheets", ["fact_sheet_category_id"], name: "index_fact_sheets_on_fact_sheet_category_id"

  create_table "locations", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "locations", ["client_id"], name: "index_locations_on_client_id"

  create_table "steps", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "step_order"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id"
    t.integer  "department_id"
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "users", ["department_id"], name: "index_users_on_department_id"
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
