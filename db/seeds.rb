# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

EthnicCode.destroy_all
EthnicCode.create([
  { title: 'Asian or Asian British – Bangladeshi' },
  { title: 'Asian or Asian British – Indian' },
  { title: 'Asian or Asian British – Pakistani' },
  { title: 'Asian or Asian British – any other Asian background' },
  { title: 'Black or Black British – African' },
  { title: 'Black or Black British – Caribbean' },
  { title: 'Black or Black British – any other black background' },
  { title: 'Chinese' },
  { title: 'Mixed – White and Asian' },
  { title: 'Mixed – White and Black African' },
  { title: 'Mixed – White and Black Caribbean' },
  { title: 'Mixed – any other mixed background' },
  { title: 'White – British' },
  { title: 'White – Irish' },
  { title: 'White – any other white background' },
  { title: 'Any other' },
  { title: 'Not known/not provided' }
])

Client.destroy_all
Client.create( # Client
 name: 'Test Client' 
)

Location.destroy_all
Client.first.locations.create( # Location
  title: 'Location 1'
)

Department.destroy_all
Client.first.locations.first.departments.create( # Department
  name: 'Department 1'
)

Step.destroy_all
Step.create([
  { title: 'Body Composition', step_order: 10 },
  { title: 'Blood Pressure', step_order: 20 },
  { title: 'Blood Sugar', step_order: 40 },
  { title: 'Cholesterol', step_order: 30 }
])


