class AddDeparmentToEmployeeCheck < ActiveRecord::Migration
  def change
    add_reference :employee_checks, :department, index: true
  end
end
