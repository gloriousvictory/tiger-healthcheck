class CreateFactSheets < ActiveRecord::Migration
  def change
    create_table :fact_sheets do |t|
      t.string :title
      t.string :pdf_file
      t.references :fact_sheet_category, index: true

      t.timestamps null: false
    end
  end
end
