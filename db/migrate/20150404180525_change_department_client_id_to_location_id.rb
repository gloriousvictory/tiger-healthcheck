class ChangeDepartmentClientIdToLocationId < ActiveRecord::Migration
  def change
    rename_column :departments, :client_id, :location_id
  end
end
