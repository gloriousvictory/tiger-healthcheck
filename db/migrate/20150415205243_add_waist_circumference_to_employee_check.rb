class AddWaistCircumferenceToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :waist_circumference, :integer
  end
end
