class AddBasicDetailsToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :first_name, :string
    add_column :employee_checks, :last_name, :string
    add_column :employee_checks, :email, :string
    add_column :employee_checks, :gender, :integer
    add_column :employee_checks, :date_of_birth, :date
    add_column :employee_checks, :basic_details_comment, :text
  end
end
