class AddWeightMeasurementsToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :weight_kg, :float
    add_column :employee_checks, :weight_stone, :integer
    add_column :employee_checks, :weight_lbs, :integer
  end
end
