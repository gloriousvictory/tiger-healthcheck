class AddCholesterolToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :total_cholesterol, :float
    add_column :employee_checks, :triglycerides, :float
    add_column :employee_checks, :hdl_cholesterol, :float
    add_column :employee_checks, :ldl_cholesterol, :float
    add_column :employee_checks, :td_hdl_ratio, :float
  end
end
