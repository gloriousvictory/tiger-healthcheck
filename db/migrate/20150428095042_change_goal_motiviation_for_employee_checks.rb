class ChangeGoalMotiviationForEmployeeChecks < ActiveRecord::Migration
  def change
    add_column :employee_checks, :body_fat_goal_1, :string
    add_column :employee_checks, :body_fat_goal_2, :string
    add_column :employee_checks, :body_fat_goal_3, :string
    add_column :employee_checks, :body_fat_motivation, :integer
    add_column :employee_checks, :blood_pressure_goal_1, :string
    add_column :employee_checks, :blood_pressure_goal_2, :string
    add_column :employee_checks, :blood_pressure_goal_3, :string
    add_column :employee_checks, :blood_pressure_motivation, :integer
    add_column :employee_checks, :blood_sugar_goal_1, :string
    add_column :employee_checks, :blood_sugar_goal_2, :string
    add_column :employee_checks, :blood_sugar_goal_3, :string
    add_column :employee_checks, :blood_sugar_motivation, :integer
    add_column :employee_checks, :total_cholesterol_goal_1, :string
    add_column :employee_checks, :total_cholesterol_goal_2, :string
    add_column :employee_checks, :total_cholesterol_goal_3, :string
    add_column :employee_checks, :total_cholesterol_motivation, :integer      
  end
end
