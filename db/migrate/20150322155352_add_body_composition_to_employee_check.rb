class AddBodyCompositionToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :body_fat, :float
    add_column :employee_checks, :water, :float
    add_column :employee_checks, :visceral_fat, :float
    add_column :employee_checks, :total_water, :float
    add_column :employee_checks, :basal_metabolic_rate, :integer
  end
end
