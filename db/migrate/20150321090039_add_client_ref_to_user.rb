class AddClientRefToUser < ActiveRecord::Migration
  def change
    add_column :users, :client_id, :integer, references: :clients
  end
end
