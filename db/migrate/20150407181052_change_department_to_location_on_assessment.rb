class ChangeDepartmentToLocationOnAssessment < ActiveRecord::Migration
  def change
    rename_column :assignments, :department_id, :location_id
  end
end
