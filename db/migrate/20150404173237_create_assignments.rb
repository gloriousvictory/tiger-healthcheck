class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.references :user, index: true
      t.references :department, index: true

      t.timestamps null: false
    end
  end
end
