class AddHeightMeasurementsToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :height_cm, :float
    add_column :employee_checks, :height_feet, :integer
    add_column :employee_checks, :height_inches, :integer
  end
end
