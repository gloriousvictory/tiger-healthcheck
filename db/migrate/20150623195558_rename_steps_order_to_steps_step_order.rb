class RenameStepsOrderToStepsStepOrder < ActiveRecord::Migration
  def change
    rename_column :steps, :order, :step_order
  end
end
