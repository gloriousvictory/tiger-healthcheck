class CreateEthnicCodes < ActiveRecord::Migration
  def change
    create_table :ethnic_codes do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
