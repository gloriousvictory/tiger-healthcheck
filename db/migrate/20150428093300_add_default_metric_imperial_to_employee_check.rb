class AddDefaultMetricImperialToEmployeeCheck < ActiveRecord::Migration
  def change
    change_column :employee_checks, :metric_imperial, :integer, :default => 0 
  end
end
