class CreateFactSheetCategories < ActiveRecord::Migration
  def change
    create_table :fact_sheet_categories do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
