class AddBloodSugarToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :blood_sugar, :float
    add_column :employee_checks, :test_type, :integer
  end
end
