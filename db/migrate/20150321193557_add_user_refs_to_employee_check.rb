class AddUserRefsToEmployeeCheck < ActiveRecord::Migration
  def change
    add_reference :employee_checks, :user, index: true
  end
end
