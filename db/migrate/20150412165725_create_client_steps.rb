class CreateClientSteps < ActiveRecord::Migration
  def change
    create_table :client_steps do |t|
      t.references :client, index: true
      t.references :step, index: true

      t.timestamps null: false
    end
  end
end
