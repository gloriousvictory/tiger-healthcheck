class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
