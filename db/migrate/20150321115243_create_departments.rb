class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.references :client, index: true
      t.string :name

      t.timestamps null: false
    end
  end
end
