class ChangeLocationToLocationIdEmployeeCheck < ActiveRecord::Migration
  def change
    rename_column :employee_checks, :location, :location_id
  end
end
