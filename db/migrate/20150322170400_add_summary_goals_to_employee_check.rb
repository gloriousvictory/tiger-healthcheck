class AddSummaryGoalsToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :bmi_goal_1, :string
    add_column :employee_checks, :bmi_goal_2, :string
    add_column :employee_checks, :bmi_goal_3, :string
    add_column :employee_checks, :bmi_motivation, :integer
    add_column :employee_checks, :bp_goal_1, :string
    add_column :employee_checks, :bp_goal_2, :string
    add_column :employee_checks, :bp_goal_3, :string
    add_column :employee_checks, :bp_motivation, :integer
    add_column :employee_checks, :rhr_goal_1, :string
    add_column :employee_checks, :rhr_goal_2, :string
    add_column :employee_checks, :rhr_goal_3, :string
    add_column :employee_checks, :rhr_motivation, :integer
  end
end
