class AddBasic2DetailsToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :ethnic_code, :integer
    add_column :employee_checks, :location, :integer
    add_column :employee_checks, :full_part_time, :integer
    add_column :employee_checks, :smoker, :boolean
    add_column :employee_checks, :past_assessment, :boolean
  end
end
