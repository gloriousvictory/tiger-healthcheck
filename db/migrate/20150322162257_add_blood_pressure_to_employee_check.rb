class AddBloodPressureToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :systolic, :integer
    add_column :employee_checks, :diastolic, :integer
    add_column :employee_checks, :resting_hr, :integer
  end
end
