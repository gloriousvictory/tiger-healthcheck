class CreateFactSheetRequests < ActiveRecord::Migration
  def change
    create_table :fact_sheet_requests do |t|
      t.references :fact_sheet, index: true
      t.references :employee_check, index: true

      t.timestamps null: false
    end
  end
end
