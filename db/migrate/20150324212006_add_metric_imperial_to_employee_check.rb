class AddMetricImperialToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :metric_imperial, :integer
  end
end
