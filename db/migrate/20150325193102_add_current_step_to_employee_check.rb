class AddCurrentStepToEmployeeCheck < ActiveRecord::Migration
  def change
    add_column :employee_checks, :current_step, :string
  end
end
