class ChangeEthnicCodeToEthnicCodeId < ActiveRecord::Migration
  def change
    rename_column :employee_checks, :ethnic_code, :ethnic_code_id
  end
end
