module EmployeeCheckStepsHelper
  def show_hide_metric_imperial(employee_check)
    case employee_check.metric_imperial # show relevant inputs
    when 'metric'
      '.employee_check_metric_imperial_metric { display: block; }'
    when 'imperial'
      '.employee_check_metric_imperial_imperial { display: block; }'      
    else # nothing defined yet, show nothing
      @return = '.employee_check_metric_imperial_metric { display: none; }'      
      @return += '.employee_check_metric_imperial_imperial { display: none; }'            
      return @return
    end
  end

  def is_step_set?(step_sym)
    @location.client.has_step?(step_sym)
  end
end
