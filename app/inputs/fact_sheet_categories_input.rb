class FactSheetCategoriesInput < SimpleForm::Inputs::CollectionInput
  def input
    collection.each do |category|
      category.fact_sheets do |fs|
        return template.check_box("fact_sheet_id", fs.id)
      end
    end
  end
end
