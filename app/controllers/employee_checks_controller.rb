class EmployeeChecksController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_employee_check, except: [:create, :index, :new]
  before_action :set_location_or_redirect
  before_action :calculate_steps, only: [:new, :create, :edit, :update]

  def index
    @employee_checks = current_user.employee_checks.where({location_id: @location})
  end

  def new
    @employee_check = current_user.employee_checks.build
  end

  def create
    @employee_check = current_user.employee_checks.build
    @employee_check.location = @location
    @employee_check.assign_attributes(employee_check_params)

    if @employee_check.save
      redirect_to location_employee_check_employee_check_step_path(:id => 'basic', :employee_check_id => @employee_check.id)
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    @employee_check.assign_attributes(employee_check_params)
    
    if @employee_check.save
      redirect_to location_employee_check_employee_check_step_path(:id => 'basic', :employee_check_id => @employee_check.id)
    else
      render 'new'
    end
  end

  def destroy
    @employee_check.destroy
    redirect_to location_employee_checks_path, notice: 'Employee check successfully deleted'
  end

  private

  def calculate_steps
    @current_step = 1
    @total_steps = get_steps_total.to_f
    @percentage_completed = @current_step/@total_steps*100    
  end

  def get_steps_total
    @location.client.get_combined_steps.count    
  end

  def employee_check_params
    params.require(:employee_check).permit(:first_name, :last_name, :gender, :date_of_birth, :email)
  end

  def set_employee_check
    @employee_check = current_user.employee_checks.find(params[:id])
  end

  def set_location_or_redirect
    unless @location = current_user.locations.find_by_id(params[:location_id])
      redirect_to root_path, notice: 'Invalid location'
    end
  end

end
