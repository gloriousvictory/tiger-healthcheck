class EmployeeCheckStepsController < ApplicationController
  include Wicked::Wizard

  before_filter :authenticate_user!  

  before_action :set_employee_check
  before_action :set_location_or_redirect
  before_action :calculate_progress, only: [:show, :update]
  prepend_before_filter :get_steps

  # move these into an enum/array so they can be dynamically called
  # steps :basic, :body_composition, :blood_pressure, :cholesterol, :blood_sugar, :summary, :fact_sheets
  # steps self.get_steps

  def show
    if current_step_index # need this so finish_wizard_path works
      @current_step = current_step_index + 2
      @total_steps = steps.count.to_f + 1
      @percentage_completed = @current_step/@total_steps*100

      case step
      when :fact_sheets # need to access the fact sheet categories, but only on the last step
        @fact_sheet_categories = FactSheetCategory.all
      end
    end

    render_wizard
  end

  def update
    @employee_check.assign_attributes(employee_check_params)
    @employee_check.current_step = step

    case step
    when :fact_sheets # need to access the fact sheet categories, but only on the last step
      @fact_sheet_categories = FactSheetCategory.all
      EmployeeCheckMailer.employee_check_report(@employee_check).deliver_now
    end

    #if @employee_check.valid?
    render_wizard @employee_check
    #else
    #  @employee_check.save!
    #end
  end

  # Part of WickedWizard, defines the end point of the wizard
  def finish_wizard_path
    location_employee_checks_path(@location)
  end

  private

  def get_steps
    self.steps  = current_user.locations.find_by_id(params[:location_id]).client.get_combined_steps
  end

  def calculate_progress
    if current_step_index # need this so finish_wizard_path works    
      @current_step = current_step_index + 2
      @total_steps = steps.count.to_f + 1
      @percentage_completed = @current_step/@total_steps*100
    end
  end

  def employee_check_params
    self.send("employee_check_#{step}_params")
  end

  def set_employee_check
    @employee_check = current_user.employee_checks.find(params[:employee_check_id])
  end

  def set_location_or_redirect
    unless @location = current_user.locations.find_by_id(params[:location_id])
      redirect_to root_path, notice: 'Invalid location'
    end
  end  

  def employee_check_basic_params
    params.require(:employee_check).permit(:ethnic_code_id, :department_id, :full_part_time, :smoker, :past_assessment)    
  end

   def employee_check_body_composition_params
    params.require(:employee_check).permit(:metric_imperial, :height_cm, :height_feet, :height_inches, :weight_kg, :weight_stone, :weight_lbs, :full_bio_elec_impedance, :body_fat, :water, :visceral_fat, :total_water, :basal_metabolic_rate, :waist_circumference)    
  end

  def employee_check_blood_pressure_params
    params.require(:employee_check).permit(:ethnic_code, :location, :full_part_time, :smoker, :past_assessment)    
  end

  def employee_check_cholesterol_params
     params.require(:employee_check).permit(:total_cholesterol, :triglycerides, :hdl_cholesterol, :ldl_cholesterol, :td_hdl_ratio)
  end

  def employee_check_blood_pressure_params
     params.require(:employee_check).permit(:systolic, :diastolic, :resting_hr)
  end  

  def employee_check_blood_sugar_params
     params.require(:employee_check).permit(:test_type, :blood_sugar)
  end

  def employee_check_summary_params
     params.require(:employee_check).permit!    
  end  

  def employee_check_fact_sheets_params
     #params.require(:employee_check).permit(fact_sheet_requests_attributes: [:id, :fact_sheet_id])
     params.require(:employee_check).permit!
  end

end
