# This is the Users landing page and lets them select their current location
class LocationsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @locations = current_user.locations
  end
end
