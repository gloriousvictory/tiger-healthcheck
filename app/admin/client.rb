ActiveAdmin.register Client do

  filter :name

  form do |f|
    f.inputs 'Details' do
      f.input :name
      f.input :steps, as: :check_boxes
    end

    f.inputs 'Locations' do
      f.has_many :locations, allow_destroy: true do |l|
        l.input :title

        l.has_many :departments do |d|
          d.input :name
        end
      end
    end

    f.actions
  end 

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, locations_attributes: [:id, :title, :_destroy, departments_attributes: [:id, :_destroy, :name]], step_ids: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  
end

  ActiveAdmin.register Location do
    belongs_to :client
  end

  ActiveAdmin.register Department do
    belongs_to :location
  end
