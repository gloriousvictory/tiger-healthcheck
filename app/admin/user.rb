ActiveAdmin.register User do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :first_name, :last_name, :email, :password, :password_confirmation, location_ids: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  
  filter :email 
  filter :first_name
  filter :last_name
  
  form do |f|
    f.inputs "Employer Details" do
      f.input :locations, as: :check_boxes
    end

    f.inputs "Account Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :password
      f.input :password_confirmation
    end

    f.actions    
  end

  index do
    selectable_column
    column :first_name
    column :last_name
    column :email
    actions
  end

  controller do
    def update
      @user = User.find(params[:id])

      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      update!
    end
  end
end
