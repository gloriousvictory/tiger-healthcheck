class Department < ActiveRecord::Base
  belongs_to :location
  has_many :assignments

  def display_name
    return "#{location.client.name} - #{location.title} - #{name}" unless location.blank? 
  end
end
