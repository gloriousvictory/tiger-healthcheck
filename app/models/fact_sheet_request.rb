class FactSheetRequest < ActiveRecord::Base
  belongs_to :fact_sheet
  belongs_to :employee_check

  validates_presence_of :fact_sheet
  validates_presence_of :employee_check
end
