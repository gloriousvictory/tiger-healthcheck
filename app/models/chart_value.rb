class ChartValue
  attr_accessor :value, :range, :min_value, :chart_length

  def get_chart_percentage
    decimal_value = value-min_value/range
    decimal_value*chart_length
  end
end
