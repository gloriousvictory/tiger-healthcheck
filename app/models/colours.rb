class Colours
  def self.green
   '10B79B' 
  end

  def self.amber
    'C3BC2F'
  end

  def self.red
    'C45838'
  end
end
