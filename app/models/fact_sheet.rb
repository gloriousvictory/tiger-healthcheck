class FactSheet < ActiveRecord::Base
  mount_uploader :pdf_file, FactSheetPdfUploader

  belongs_to :fact_sheet_category

  validates_presence_of :title
  validates_presence_of :pdf_file
end
