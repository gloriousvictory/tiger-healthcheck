class BloodSugarReportColour
  def self.get_report_colour(rating)
    case rating
    when "Undesirable"
      Colours.red
    when "Desirable"
      Colours.green
    when "Increased Risk"
      Colours.amber
    when "Undesirable"
      Colours.red
    end
  end
end
