class BodyFatRating
  attr_accessor :gender, :age, :total_body_fat

  def get_rating
    case gender
    when 'male'
      get_rating_male
    when 'female'
      get_rating_female
    end
  end

  def get_rating_male
    return get_rating_male_cat_1 if age >= 20 && age <= 39
    return get_rating_male_cat_2 if age >= 40 && age <= 59
    return get_rating_male_cat_3 if age >= 60 && age <= 79
  end

  def get_rating_female
    return get_rating_female_cat_1 if age >= 20 && age <= 39
    return get_rating_female_cat_2 if age >= 40 && age <= 59
    return get_rating_female_cat_3 if age >= 60 && age <= 79
  end

  # Male ratings

  def get_rating_male_cat_1 # 20 to 39
    return 'Under' if total_body_fat <= 7
    return 'Healthy' if total_body_fat > 7 && total_body_fat <= 19
    return 'Overfat' if total_body_fat >= 20 && total_body_fat <= 25
    return 'Obese' if total_body_fat > 25
  end

  def get_rating_male_cat_2 # 40 to 59
    return 'Under' if total_body_fat <= 10
    return 'Healthy' if total_body_fat > 10 && total_body_fat <= 22
    return 'Overfat' if total_body_fat >= 23 && total_body_fat <= 28
    return 'Obese' if total_body_fat > 28
  end

  def get_rating_male_cat_3 # 60 to 79
    return 'Under' if total_body_fat <= 12
    return 'Healthy' if total_body_fat > 12 && total_body_fat <= 25
    return 'Overfat' if total_body_fat >= 26 && total_body_fat <= 30
    return 'Obese' if total_body_fat > 30
  end

  # Female ratings

  def get_rating_female_cat_1 # 20 to 39
    return 'Under' if total_body_fat <= 21
    return 'Healthy' if total_body_fat > 21 && total_body_fat <= 33
    return 'Overfat' if total_body_fat >= 34 && total_body_fat <= 39
    return 'Obese' if total_body_fat > 39
  end

  def get_rating_female_cat_2 # 40 to 59
    return 'Under' if total_body_fat <= 23
    return 'Healthy' if total_body_fat > 23 && total_body_fat <= 34
    return 'Overfat' if total_body_fat >= 35 && total_body_fat <= 40
    return 'Obese' if total_body_fat > 40
  end

  def get_rating_female_cat_3 # 60 to 79
    return 'Under' if total_body_fat <= 24
    return 'Healthy' if total_body_fat > 24 && total_body_fat <= 35
    return 'Overfat' if total_body_fat >= 36 && total_body_fat <= 42
    return 'Obese' if total_body_fat > 42
  end   
end
