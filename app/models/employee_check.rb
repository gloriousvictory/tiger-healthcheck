class EmployeeCheck < ActiveRecord::Base
  belongs_to :user
  has_many :fact_sheet_requests
  has_many :fact_sheets, through: :fact_sheet_requests
  belongs_to :ethnic_code
  belongs_to :location
  belongs_to :department

  accepts_nested_attributes_for :fact_sheet_requests, :reject_if => :all_blank

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :gender
  validates_presence_of :date_of_birth
  validates_presence_of :email

  validates_presence_of :ethnic_code, if: :is_step_basic?
  validates_presence_of :department, if: :is_step_basic?
  validates_presence_of :full_part_time, if: :is_step_basic?
  validates_inclusion_of :smoker, :in => [true, false], if: :is_step_basic?
  validates_inclusion_of :past_assessment, :in => [true, false], if: :is_step_basic?

  #validates_presence_of :body_fat, if: :is_step_body_composition?
  #validates_presence_of :water, if: :is_step_body_composition?
  #validates_presence_of :visceral_fat, if: :is_step_body_composition?
  #validates_presence_of :total_water, if: :is_step_body_composition?
  #validates_presence_of :basal_metabolic_rate, if: :is_step_body_composition?

  validates_presence_of :metric_imperial, if: :is_step_body_composition?
  validates_presence_of :height_cm, if: :validate_metric?
  validates_presence_of :weight_kg, if: :validate_metric?

  validates_presence_of :height_feet, if: :validate_imperial?
  validates_presence_of :height_inches, if: :validate_imperial?

  validates_presence_of :weight_stone, if: :validate_imperial?
  validates_presence_of :weight_lbs, if: :validate_imperial?

  validates_presence_of :systolic, if: :is_step_blood_pressure?
  validates_presence_of :diastolic, if: :is_step_blood_pressure?
  validates_presence_of :resting_hr, if: :is_step_blood_pressure?

  #validates_presence_of :total_cholesterol, if: :is_step_cholesterol?
  #validates_presence_of :triglycerides, if: :is_step_cholesterol?
  #validates_presence_of :hdl_cholesterol, if: :is_step_cholesterol?
  #validates_presence_of :ldl_cholesterol, if: :is_step_cholesterol?
  #validates_presence_of :td_hdl_ratio, if: :is_step_cholesterol?

  validates_presence_of :test_type, if: :is_step_blood_sugar?
  validates_presence_of :blood_sugar, if: :is_step_blood_sugar?

  enum gender: [:male, :female]
  enum full_part_time: [:full_time, :part_time]
  enum metric_imperial: [:metric, :imperial]
  enum test_type: [:fasting, :non_fasting]

  # Default by date descending
  default_scope { order('created_at DESC') }

  def created_date_string
    created_at.strftime("%e %B %Y")
  end

  def full_name
    return "#{first_name} #{last_name}"
  end

  def get_bmi
    return get_bmi_metric if metric?
    return get_bmi_imperial if imperial?
  end

  def get_bmi_imperial
    # weight (lbs) * 703 divided by height (inches sqr)
    total_height_inches = (height_feet * 12) + height_inches
    height = total_height_inches ** 2

    total_weight_lbs = (weight_stone * 14) + weight_lbs
    weight = total_weight_lbs * 703

    bmi = weight.to_f/height.to_f
    BigDecimal.new(bmi, 4).to_f
  end

  def get_bmi_metric
    #  weight (kg) divided by height (m sqr)
    
    # get height in metres sqr.
    height = (height_cm/100) ** 2

    # get weight in kg
    weight = weight_kg

    bmi = weight.to_f/height.to_f
    BigDecimal.new(bmi, 4).to_f    
  end

  def get_bmi_rating
    @bmi_rating = BmiRating.new
    @bmi_rating.bmi = get_bmi
    @bmi_rating.get_rating
  end

  def get_resting_heart_rate_rating
    @resting_heart_rate_rating = RestingHeartRateRating.new
    @resting_heart_rate_rating.resting_hr = resting_hr

    @resting_heart_rate_rating.get_rating
  end

  def get_total_body_fat_rating
    @body_fat_rating = BodyFatRating.new
    @body_fat_rating.gender = gender
    @body_fat_rating.age = get_age
    @body_fat_rating.total_body_fat = body_fat
    @body_fat_rating.get_rating
  end

  def get_total_cholesterol_rating
    @total_cholesterol_rating = TotalCholesterolRating.new
    @total_cholesterol_rating.total_cholesterol = total_cholesterol

    @total_cholesterol_rating.get_rating
  end

  def get_hdl_cholesterol_rating
    @hdl_cholesterol_rating = HdlCholesterolRating.new
    @hdl_cholesterol_rating.gender = gender
    @hdl_cholesterol_rating.hdl_cholesterol_value = hdl_cholesterol

    @hdl_cholesterol_rating.get_rating
  end

  def get_total_cholesterol_hdl_ratio_rating
    @total_cholesterol_hdl_ratio_rating = TotalCholesterolHdlRatioRating.new
    @total_cholesterol_hdl_ratio_rating.total_cholesterol_ratio = get_total_cholesterol_hdl_ratio
    
    @total_cholesterol_hdl_ratio_rating.get_rating
  end

  def get_blood_pressure_rating
    @blood_pressure_rating = BloodPressureRating.new
    @blood_pressure_rating.systolic = systolic
    @blood_pressure_rating.diastolic = diastolic
    @blood_pressure_rating.get_rating
  end

  def get_blood_sugar_rating
    @blood_sugar_rating = BloodSugarRating.new
    @blood_sugar_rating.blood_sugar = blood_sugar
    @blood_sugar_rating.get_rating 
  end

  def has_total_cholesterol
    total_cholesterol.present?
  end  

  def has_hdl_cholesterol
    hdl_cholesterol.present?
  end

  def has_ldl_cholesterol
    ldl_cholesterol.present?
  end

  private
  def get_total_cholesterol_hdl_ratio
    ratio = total_cholesterol/hdl_cholesterol
    BigDecimal.new(ratio, 3).to_f
  end  

  def get_age
    now = Time.now.utc
    return now.year - date_of_birth.year - (date_of_birth.to_time.change(:year => now.year) > now ? 1 : 0)
  end

  def validate_metric?
    is_step_body_composition? && metric_imperial.eql?('metric')
  end

  def validate_imperial?
    is_step_body_composition? && metric_imperial.eql?('imperial')
  end

  def is_step_basic?
    is_current_step?('basic')
  end

  def is_step_body_composition?
    is_current_step?('body_composition')
  end

  def is_step_blood_pressure?
    is_current_step?('blood_pressure')
  end

  def is_step_cholesterol?
    is_current_step?('cholesterol')
  end

  def is_step_blood_sugar?
    is_current_step?('blood_sugar')
  end    

  def is_current_step?(_current_step)
    current_step.eql?(_current_step)
  end

end
