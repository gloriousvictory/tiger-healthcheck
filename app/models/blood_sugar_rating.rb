class BloodSugarRating
  attr_accessor :blood_sugar

  def get_rating
    raise 'No blood sugar value set' if blood_sugar.blank?

    return 'Undesirable' if blood_sugar < 4 
    return 'Desirable' if blood_sugar >= 4 && blood_sugar < 8
    return 'Increased Risk' if blood_sugar >= 8 && blood_sugar < 11
    return 'Undesirable' if blood_sugar >= 11    
  end
end
