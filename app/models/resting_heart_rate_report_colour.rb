class RestingHeartRateReportColour
  def self.get_report_colour(resting_heart_rate_rating)
    case resting_heart_rate_rating
    when "Excellent"
      Colours.green
    when "Good"
      Colours.green
    when "Average"
      Colours.amber
    when "Poor"
      Colours.red
    end
  end
end
