class BmiReportColour
  def self.get_report_colour(bmi_rating)
    case bmi_rating
    when "Underweight"
      return Colours.amber
    when "Normal Range"
      return Colours.green
    when "Overweight"
      return Colours.amber
    when "Obese Class 1"
      return Colours.red
    when "Obese Class 2"
      return Colours.red
    when "Obese Class 3"
      return Colours.red
    end
  end
end
