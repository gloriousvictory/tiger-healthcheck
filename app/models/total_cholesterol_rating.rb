class TotalCholesterolRating
  attr_accessor :total_cholesterol
  
  def get_rating
    raise 'total_cholesterol not set' if total_cholesterol.blank?

    return 'Ideal' if total_cholesterol < 4
    return 'Desirable' if total_cholesterol >= 4 && total_cholesterol < 5
    return 'Increased Risk' if total_cholesterol >= 5 && total_cholesterol <= 6
    return 'Undesirable' if total_cholesterol > 6
  end
end
