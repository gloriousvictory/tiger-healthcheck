class BmiRating
  attr_accessor :bmi

  def get_rating
    raise 'bmi not set' if bmi.blank?

    return "Underweight" if bmi <= 18.5
    return "Normal Range" if bmi > 18.5 && bmi <= 24.99
    return "Overweight" if bmi >= 25 && bmi <= 29.99
    return "Obese Class 1" if bmi >= 30 && bmi <= 34.99
    return "Obese Class 2" if bmi >= 35 && bmi <= 39.99
    return "Obese Class 3" if bmi >= 40    
  end

  def get_chart_value
    raise 'bmi not set' if bmi.blank?
  end
end
