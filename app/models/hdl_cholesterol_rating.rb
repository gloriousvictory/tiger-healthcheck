class HdlCholesterolRating
  attr_accessor :gender, :hdl_cholesterol_value

  def get_rating
    raise 'No gender set' if gender.blank?
    raise 'No hdl_cholesterol_value set' if hdl_cholesterol_value.blank?

    case gender
    when 'male'
      return 'Desirable' if hdl_cholesterol_value >= 1
      return 'Undesirable' if hdl_cholesterol_value < 1
    when 'female'
      return 'Desirable' if hdl_cholesterol_value >= 1.2
      return 'Undesirable' if hdl_cholesterol_value < 1.2     
    end    
  end
end
