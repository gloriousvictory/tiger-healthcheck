class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :validatable

  has_many :assignments
  has_many :employee_checks

  has_many :locations, through: :assignments
  has_many :clients, through: :locations

  accepts_nested_attributes_for :assignments, :allow_destroy => true,:reject_if => :all_blank

  def full_name
    return "#{first_name} #{last_name}"
  end

end
