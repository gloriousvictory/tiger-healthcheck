class Location < ActiveRecord::Base
  belongs_to :client
  has_many :departments, :dependent => :delete_all
  has_many :employee_checks

  accepts_nested_attributes_for :departments, allow_destroy: true

  def display_name
    get_client_and_location
  end

  def get_client_and_location
    "#{client.name} - #{title}" unless client.blank?
  end
end
