class TotalCholesterolReportColour
  def self.get_report_colour(rating)
    case rating
    when "Ideal"
      Colours.green
    when "Desirable"
      Colours.green
    when "Increased Risk"
      Colours.amber
    when "Undesirable"
      Colours.red
    end
  end
end
