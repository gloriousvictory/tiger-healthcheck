class FactSheetCategory < ActiveRecord::Base
  has_many :fact_sheets

  validates_presence_of :title
end
