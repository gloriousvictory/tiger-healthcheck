require 'prawn'

class ReportGenerator
  DEFAULT_PADDING = 25
  H1_FONT_SIZE = 39
  H2_FONT_SIZE = 32
  H3_FONT_SIZE = 26
  H4_FONT_SIZE = 22

  def generate_report(employee_check)
    pdf = Prawn::Document.new(:page_size => 'A4', :margin => [0,0,0,0])

    pdf.font_families.update("Lato" => {
        :normal => "#{Rails.root}/app/assets/fonts/Lato-Regular.ttf",
        :italic => "#{Rails.root}/app/assets/fonts/Lato-Italic.ttf",
        :bold => "#{Rails.root}/app/assets/fonts/Lato-Bold.ttf",
        :bold_italic => "#{Rails.root}/app/assets/fonts/Lato-BoldItalic.ttf",
        :light => "#{Rails.root}/app/assets/fonts/Lato-Light.ttf"
    })

    pdf.font "Lato"
    
    pdf.fill_color "ffffff" # setting this here for clarity    

    pdf = render_front_cover(pdf, employee_check)
    pdf.fill_color "2c3e50" # setting this here for clarity
    pdf = render_introduction(pdf)

    # There has to be a better way to chain/separate these out?
    pdf = render_bmi(pdf, employee_check) if employee_check.location.client.has_step?(:body_composition)
    pdf = render_blood_pressure(pdf, employee_check) if employee_check.location.client.has_step?(:blood_pressure)
    pdf = render_resting_heart_rate(pdf, employee_check) if employee_check.location.client.has_step?(:blood_pressure)
    pdf = render_body_fat(pdf, employee_check) if employee_check.location.client.has_step?(:body_composition)
    pdf = render_blood_sugar(pdf, employee_check) if employee_check.location.client.has_step?(:blood_sugar)
    pdf = render_cholesterol(pdf, employee_check) if employee_check.location.client.has_step?(:cholesterol)

    #return pdf.render_file "test.pdf" unless Rails.env.production? # render to file in dev/test
    pdf.render # return object rather than path in production
  end

  private

  def render_front_cover(pdf, employee_check)
    pdf.image "#{Rails.root}/app/assets/images/report-bg.jpg", :at  => [0, Prawn::Document::PageGeometry::SIZES["A4"][1]]
    pdf.indent(40) do
      pdf.move_down 40 
      pdf.text "Health Assessment", :size => 40
      pdf.text "Report", :size => 50, :style => :italic
      pdf.move_down 30
      pdf.text "For #{employee_check.full_name}", :size => H3_FONT_SIZE, :style => :light
      pdf.text "#{employee_check.created_date_string}", :size => H3_FONT_SIZE, :style => :light
    end

    pdf.image "#{Rails.root}/app/assets/images/LGH-logo-white-outline.png", :at  => [Prawn::Document::PageGeometry::SIZES["A4"][0]-260, 180] # image size is 220x140
    
    pdf
  end

  def render_introduction(pdf)
    pdf.start_new_page(:margin => 40)    
    pdf.text "Welcome to your health assessment report and congratulations on taking your first steps to better health and an improved healthy lifestyle.", :leading => 1.5

    pdf.move_down 10    

    pdf.text "Let’s Get Healthy appreciate that changing your lifestyle can be difficult and so we have tried to make it as simple for you as possible. There are three easy steps to better health.", :leading => 1.5

    pdf.move_down 10
    
    pdf.table([
        ["•", "<b>Education</b> - You have completed your in depth health assessment and you now know the areas of health improvement required as well as what you are doing well"],
        ["•", "<b>Ownership of you goals</b> - Now that you have them written down, spend time looking at them each week and review your success. When you attend your next health assessment your health leader will follow up on these goals and review them with you"],
        ["•", "<b>Expert support</b> -  Relevant fact sheets are provided offering realistic and handy hints and tips around your key health concerns and interests"]
    ], cell_style: { borders: [], :inline_format => true  })

    pdf.move_down 10

    pdf.text "Your report is based around <b>simplicity</b>. You will find it is easy to navigate and is written in a way that you are able to understand. You will notice that results are categorised in one of three simple outcomes:", :leading => 1.5, :inline_format => true

    pdf.move_down 10    

    pdf.table([
        ["•", "<b>Green</b> = Excellent! No improvement required"],
        ["•", "<b>Amber</b> = Not bad but can benefit from improvement in this area" ],
        ["•", "<b>Red</b> = Let's investigate this area a little closer"]
    ], cell_style: { borders: [], :inline_format => true })

    pdf.move_down 10    

    pdf.text "Please be aware that the results from your health assessment are strictly confidential, however from time to time we may include your results in group data that we report back but you will never be identifiable as an individual.", :leading => 1.5

    pdf.move_down 10
    
    pdf.text "Once again well done on taking the first steps to better health and we hope that you enjoy your continued health journey.", :leading => 1.5
    pdf
  end
  
  def render_bmi(pdf, employee_check)
    pdf.start_new_page(:margin => 40)
    
    pdf.text "Body Mass Index", :size => H2_FONT_SIZE
    pdf.text "The Body Mass Index (BMI) rating is an indicator of total body composition, or body weight. It is calculated by dividing your weight (kg) by your height in meters squared. A healthy BMI for an adult is between 18.5 and 25.", :leading => 1.5
    pdf.move_down 20
    pdf.text "BMI calculations will be overestimated for people with high muscle mass, very active people and pregnant women so this may not reflect a healthy weight as BMI does not differentiate between body fat and muscle weight.", :leading => 1.5

    pdf.move_down DEFAULT_PADDING
    
    pdf.text "BMI Value", :size => H3_FONT_SIZE
    pdf.text employee_check.get_bmi.to_s
    pdf.move_down DEFAULT_PADDING

    pdf.text "Rating", :size => H3_FONT_SIZE
    rating = employee_check.get_bmi_rating
    render_rating(pdf, rating, BmiReportColour.get_report_colour(rating))

    pdf.text "Goals and Motivation", :size => H3_FONT_SIZE
    pdf.move_down 10    
    
    if employee_check.bmi_goal_1.present?
      pdf.text "My first goal", :size => H4_FONT_SIZE
      pdf.text employee_check.bmi_goal_1, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    
    if employee_check.bmi_goal_2.present?
      pdf.text "My second goal", :size => H4_FONT_SIZE 
      pdf.text employee_check.bmi_goal_2, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    
    if employee_check.bmi_goal_3.present?
      pdf.text "My third goal", :size => H4_FONT_SIZE
      pdf.text employee_check.bmi_goal_3, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end

    pdf.text "Motivation: #{employee_check.bmi_motivation}/10", :size => H4_FONT_SIZE if employee_check.bmi_motivation
    
    pdf    
  end

  def render_blood_pressure(pdf, employee_check)
    pdf.start_new_page(:margin => 40)

    pdf.text "Blood Pressure", :size => H2_FONT_SIZE
    pdf.text "Blood pressure is the measure of the force that the heart needs to pump the blood around the body. There are two different measures of pressure. Systolic measures the pumping or beating phase of the heart. Diastolic measures the relaxation phase when the heart is filling up with blood.", :leading => 1.5

    pdf.move_down 20
    pdf.text "Blood pressure can vary throughout the day and can be affected by stress levels, smoking and caffeine intake. High BP put increased strain on the heart and may lead to heart attacks or strokes.", :leading => 1.5

    pdf.move_down DEFAULT_PADDING
    
    pdf.text "Systolic BP", :size => H3_FONT_SIZE
    pdf.text "#{employee_check.systolic.to_s} mm Hg"
    pdf.move_down DEFAULT_PADDING

    pdf.text "Diastolic BP", :size => H3_FONT_SIZE
    pdf.text "#{employee_check.diastolic.to_s} mm Hg"
    pdf.move_down DEFAULT_PADDING    

    pdf.text "Rating", :size => H3_FONT_SIZE
    rating = employee_check.get_blood_pressure_rating.to_s
    render_rating(pdf, rating, BloodPressureReportColour.get_report_colour(rating))

    pdf.text "Goals and Motivation", :size => H3_FONT_SIZE
    pdf.move_down 10    
   
    if employee_check.bp_goal_1.present?
      pdf.text "My first goal", :size => H4_FONT_SIZE
      pdf.text employee_check.bp_goal_1, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    
    if employee_check.bp_goal_2.present?
      pdf.text "My second goal", :size => H4_FONT_SIZE
      pdf.text employee_check.bp_goal_2, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    
    if employee_check.bp_goal_3.present?
      pdf.text "My third goal", :size => H4_FONT_SIZE
      pdf.text employee_check.bp_goal_3, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    
    pdf.text "Motivation: #{employee_check.bp_motivation}/10", :size => H4_FONT_SIZE if employee_check.bp_motivation

    pdf    
  end

  def render_resting_heart_rate(pdf, employee_check)
    pdf.start_new_page(:margin => 40)

    pdf.text "Resting Heart Rate", :size => H2_FONT_SIZE
    pdf.text "The number of times your heart beats in one minute when you are at complete rest. The fitter you are the less times your heart has to beat to pump the blood around your body.", :leading => 1.5
    pdf.move_down DEFAULT_PADDING
    
    pdf.text "Resting Heart Rate", :size => H3_FONT_SIZE
    pdf.text "#{employee_check.resting_hr.to_s} bpm"
    pdf.move_down DEFAULT_PADDING

    pdf.text "Rating", :size => H3_FONT_SIZE
    rhr_rating = employee_check.get_resting_heart_rate_rating
    render_rating(pdf, rhr_rating, RestingHeartRateReportColour.get_report_colour(rhr_rating))    

    pdf.text "Goals and Motivation", :size => H3_FONT_SIZE
    pdf.move_down 10    
    
    if employee_check.rhr_goal_1.present?
      pdf.text "My first goal", :size => H4_FONT_SIZE 
      pdf.text employee_check.rhr_goal_1, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    
    if employee_check.rhr_goal_2.present?
      pdf.text "My second goal", :size => H4_FONT_SIZE
      pdf.text employee_check.rhr_goal_2, :leading => 1.5 
      pdf.move_down DEFAULT_PADDING
    end
    
    if employee_check.rhr_goal_3.present?
      pdf.text "My third goal", :size => H4_FONT_SIZE 
      pdf.text employee_check.rhr_goal_3, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end
    

    pdf.text "Motivation: #{employee_check.rhr_motivation}/10", :size => H4_FONT_SIZE if employee_check.rhr_motivation
    pdf    
  end

  def render_body_fat(pdf, employee_check)
    if employee_check.body_fat.present?
      pdf.start_new_page(:margin => 40) 

      pdf.text "Body Fat", :size => H2_FONT_SIZE
      pdf.text "Body fat% is the amount of body fat as a proportion of your total body weight. Lower levels of body fat can reduce the risk of certain conditions such as high blood pressure and type 2 diabetes.", :leading => 1.5
      pdf.move_down DEFAULT_PADDING
      
      pdf.text "Body Fat Value", :size => H3_FONT_SIZE
      pdf.text "#{employee_check.body_fat.to_s}%"
      pdf.move_down DEFAULT_PADDING

      pdf.text "Rating", :size => H3_FONT_SIZE
      rating =  employee_check.get_total_body_fat_rating.to_s
      render_rating(pdf, rating, BodyFatReportColour.get_report_colour(rating)) 
      
      pdf.text "Goals and Motivation", :size => H3_FONT_SIZE
      pdf.move_down 10      
     
      if employee_check.body_fat_goal_1.present?
        pdf.text "My first goal", :size => H4_FONT_SIZE
        pdf.text employee_check.body_fat_goal_1, :leading => 1.5
        pdf.move_down DEFAULT_PADDING
      end
      
      if employee_check.body_fat_goal_2.present?
        pdf.text "My second goal", :size => H4_FONT_SIZE
        pdf.text employee_check.body_fat_goal_2, :leading => 1.5
        pdf.move_down DEFAULT_PADDING
      end
      
      if employee_check.body_fat_goal_3.present?
        pdf.text "My third goal", :size => H4_FONT_SIZE
        pdf.text employee_check.body_fat_goal_3, :leading => 1.5
        pdf.move_down DEFAULT_PADDING
      end 

      pdf.text "Motivation: #{employee_check.body_fat_motivation}/10", :size => H4_FONT_SIZE if employee_check.body_fat_motivation
    end

    pdf    
  end

  def render_blood_sugar(pdf, employee_check)
    pdf.start_new_page(:margin => 40)    

    pdf.text "Blood Sugar", :size => H2_FONT_SIZE

    pdf.text "Blood sugar or glucose is a measure of how much sugar is being carried in the bloodstream. High blood sugar over a period of time may lead to conditions such as type 2 diabetes. This is often due to excess body fat, lack of physical activity and a diet containing high sugar carbohydrates.", :leading => 1.5

    pdf.move_down DEFAULT_PADDING
    
    pdf.text "Blood Sugar Value", :size => H3_FONT_SIZE
    pdf.text employee_check.blood_sugar.to_s
    pdf.move_down DEFAULT_PADDING

    pdf.text "Rating", :size => H3_FONT_SIZE

    rating = employee_check.get_blood_sugar_rating.to_s
    render_rating(pdf, rating, BloodSugarReportColour.get_report_colour(rating)) 

    pdf.text "Goals and Motivation", :size => H3_FONT_SIZE
    pdf.move_down 10    
    
    if employee_check.blood_sugar_goal_1.present?
      pdf.text "My first goal", :size => H4_FONT_SIZE
      pdf.text employee_check.blood_sugar_goal_1, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end

    if employee_check.blood_sugar_goal_2.present?
      pdf.text "My second goal", :size => H4_FONT_SIZE
      pdf.text employee_check.blood_sugar_goal_2, :leading => 1.5
      pdf.move_down DEFAULT_PADDING
    end

    if employee_check.blood_sugar_goal_3.present?
      pdf.text "My third goal", :size => H4_FONT_SIZE
      pdf.text employee_check.blood_sugar_goal_3, :leading => 1.5    
      pdf.move_down DEFAULT_PADDING
    end

    pdf.text "Motivation: #{employee_check.blood_sugar_motivation}/10", :size => H4_FONT_SIZE if employee_check.blood_sugar_motivation
    pdf    
  end

  def render_cholesterol(pdf, employee_check)
    if employee_check.has_total_cholesterol # not guaranteed to have a value to use...
      pdf.start_new_page(:margin => 40)

      pdf.text "Cholesterol", :size => H2_FONT_SIZE
      pdf.text "Cholesterol is a waxy substance which is made in the body by the liver. It is also found foods such as meat, seafood and dairy products. It is vital for various functions in the body such as cell growth and nerve insulation. Too much cholesterol in the body can increase your risk of cardio vascular conditions such as heart attack or stroke.", :leading => 1.5

      pdf.move_down DEFAULT_PADDING
      
      pdf.text "Total Cholesterol Value", :size => H3_FONT_SIZE
      pdf.text employee_check.total_cholesterol.to_s
      pdf.move_down DEFAULT_PADDING

      pdf.text "Rating", :size => H3_FONT_SIZE
      rating = employee_check.get_total_cholesterol_rating.to_s
      render_rating(pdf, rating, TotalCholesterolReportColour.get_report_colour(rating))

      pdf.text "Goals and Motivation", :size => H3_FONT_SIZE
      pdf.move_down 10

      if employee_check.total_cholesterol_goal_1.present?
        pdf.text "My first goal", :size => H4_FONT_SIZE
        pdf.text employee_check.total_cholesterol_goal_1, :leading => 1.5 
        pdf.move_down DEFAULT_PADDING
      end

      if employee_check.total_cholesterol_goal_2.present?
        pdf.text "My second goal", :size => H4_FONT_SIZE 
        pdf.text employee_check.total_cholesterol_goal_2, :leading => 1.5 
        pdf.move_down DEFAULT_PADDING
      end

      if employee_check.total_cholesterol_goal_3.present?
        pdf.text "My third goal", :size => H4_FONT_SIZE 
        pdf.text employee_check.total_cholesterol_goal_3, :leading => 1.5 
        pdf.move_down DEFAULT_PADDING      
      end

      pdf.text "Motivation: #{employee_check.total_cholesterol_motivation}/10", :size => H4_FONT_SIZE if employee_check.total_cholesterol_motivation
    end   

    pdf    
  end

  def render_rating(pdf, rating_value, rating_colour)
    pdf.table([[rating_value]]) do
      cells.style.border_width = 0
      cells.style.padding = 20
      cells.style.align = :center
      column(0).text_color = 'ffffff'
      column(0).background_color = rating_colour
    end

    pdf.move_down DEFAULT_PADDING

    pdf    
  end
end
