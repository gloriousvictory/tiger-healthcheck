class TotalCholesterolHdlRatioRating
  attr_accessor :total_cholesterol_ratio

  def get_rating
    raise 'total_cholesterol_ratio not set' if total_cholesterol_ratio.blank?

    return 'Ideal' if total_cholesterol_ratio < 5 
    return 'Undesirable' if total_cholesterol_ratio >= 5
  end
end
