class BodyFatReportColour
  def self.get_report_colour(body_fat_rating)
    case body_fat_rating
    when "Under"
      Colours.amber
    when "Healthy"
      Colours.green
    when "Overfat"
      Colours.amber
    when "Obese"
      Colours.red
    end
  end
end
