class Client < ActiveRecord::Base
  has_many :users
  has_many :locations, :dependent => :delete_all
  has_many :departments, through: :locations

  has_many :client_steps
  has_many :steps, through: :client_steps

  validates_presence_of :name
  validates_uniqueness_of :name

  accepts_nested_attributes_for :locations, allow_destroy: true
  accepts_nested_attributes_for :client_steps, :reject_if => :all_blank

  def get_steps
    steps.map {|s| s.to_sym}
  end

  def get_combined_steps
    steps = get_steps
    steps.unshift :basic # add to start
    steps << :summary # add to end
    steps << :fact_sheets # add to end

    steps
  end

  def has_step?(step)
    get_combined_steps.include?(step)
  end 
end
