class Step < ActiveRecord::Base
  has_many :client_steps

  validates_presence_of :title
  validates_presence_of :step_order

  default_scope { order('step_order ASC') }

  def to_sym
    title.parameterize.underscore.to_sym
  end
end
