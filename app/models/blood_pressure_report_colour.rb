class BloodPressureReportColour
  def self.get_report_colour(rating)
    case rating
    when "Optimal Blood Pressure"
      Colours.green
    when "Normal Blood Pressure"
      Colours.green
    when "High-Normal Blood Pressure"
      Colours.amber
    when "Grade 1 Hypertension"
      Colours.red
    when "Grade 2 Hypertension"
      Colours.red
    when "Grade 3 Hypertension"
      Colours.red
    end
  end
end
