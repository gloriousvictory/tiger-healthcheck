class BloodPressureRating
  attr_accessor :systolic, :diastolic

  def get_rating
    raise 'systolic not set' if systolic.blank?
    raise 'diastolic not set' if diastolic.blank?

    return 'Optimal Blood Pressure' if systolic <= 120 && diastolic <= 80
    return 'Normal Blood Pressure' if systolic <= 130 && diastolic <= 85
    return 'High-Normal Blood Pressure' if systolic > 130 && systolic < 140 && diastolic > 85 && diastolic < 90
    return 'Grade 1 Hypertension' if systolic >= 140 && systolic < 160 && diastolic >= 90 && diastolic < 100
    return 'Grade 2 Hypertension' if systolic >= 160 && systolic < 180 && diastolic >= 100 && diastolic < 110
    return 'Grade 3 Hypertension' if systolic >= 180 && diastolic >= 110
  end
end
