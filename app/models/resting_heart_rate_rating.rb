class RestingHeartRateRating
  attr_accessor :resting_hr

  def get_rating
    raise 'resting_hr not set' if resting_hr.blank?

    return "Excellent" if resting_hr <= 55
    return "Good" if resting_hr > 55 && resting_hr <= 69
    return "Average" if resting_hr >= 70 && resting_hr <= 84
    return "Poor" if resting_hr >= 85    
  end
end
