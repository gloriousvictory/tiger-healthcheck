class EmployeeCheckMailer < ApplicationMailer
  default from: 'results@letsgethealthy.co.uk'

  def employee_check_report(employee_check)
    employee_check.fact_sheet_requests.each do |fsr|
      next if fsr.fact_sheet.nil?
      attachments["#{fsr.fact_sheet.title}.pdf"] = fsr.fact_sheet.pdf_file.read
    end

    @report_generator = ReportGenerator.new # probably should move this to a static?
    attachments["Healthcheck-report.pdf"] = @report_generator.generate_report(employee_check)    

    mail(to: employee_check.email, subject: 'Healthcheck Report')
  end
end
