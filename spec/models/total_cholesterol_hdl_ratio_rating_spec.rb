require 'rails_helper'

RSpec.describe TotalCholesterolHdlRatioRating, type: :model do
  describe '#get_rating' do
    context 'total_cholesterol_ratio is not set' do
      let(:subject) { FactoryGirl.build(:total_cholesterol_hdl_ratio_rating) }      
      
      it 'raises error' do
        expect(lambda { subject.get_rating }).to raise_error(RuntimeError)
      end
    end

    context 'total_cholesterol_ratio is 4.9' do
      let(:subject) { FactoryGirl.build(:total_cholesterol_hdl_ratio_rating, total_cholesterol_ratio: 4.9) }

      it 'returns Ideal' do
        expect(subject.get_rating).to eq 'Ideal'        
      end
    end

    context 'total_cholesterol_ratio is 5' do
      let(:subject) { FactoryGirl.build(:total_cholesterol_hdl_ratio_rating, total_cholesterol_ratio: 5) }
      
      it 'returns Undesirable' do
        expect(subject.get_rating).to eq 'Undesirable'        
      end
    end

    context 'total_cholesterol_ratio is 5.1' do
      let(:subject) { FactoryGirl.build(:total_cholesterol_hdl_ratio_rating, total_cholesterol_ratio: 5.1) }
      
      it 'returns Undesirable' do
        expect(subject.get_rating).to eq 'Undesirable'        
      end
    end    

  end
end
