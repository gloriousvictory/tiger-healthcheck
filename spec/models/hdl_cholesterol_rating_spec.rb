require 'rails_helper'

RSpec.describe HdlCholesterolRating, type: :model do
  describe '#get_rating' do
    context 'gender is blank' do
      let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating) }      
      
      it 'throws exception' do
        expect(lambda { subject.get_rating }).to raise_error(RuntimeError)        
      end
    end

    context 'hdl_cholesterol_value is blank' do
      let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating) }      
      
      it 'throws exception' do
        expect(lambda { subject.get_rating }).to raise_error(RuntimeError)        
      end
    end

    context 'gender is male' do
      context 'hdl_cholesterol_value is 0.9' do
        let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating, gender: 'male', hdl_cholesterol_value: 0.9) }      
        
        it 'returns Undesirable' do
          expect(subject.get_rating).to eq 'Undesirable'          
        end
      end

      context 'hdl_cholesterol_value is 1' do
        let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating, gender: 'male', hdl_cholesterol_value: 1) }        
        it 'returns Desirable' do
          expect(subject.get_rating).to eq 'Desirable'                    
        end
      end

      context 'hdl_cholesterol_value is 1.1' do
        let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating, gender: 'male', hdl_cholesterol_value: 1.1) }        
        it 'return Desirable' do
          expect(subject.get_rating).to eq 'Desirable'                    
        end
      end
    end

    context 'gender is female' do
      context 'hdl_cholesterol_value is 1.1' do
        let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating, gender: 'female', hdl_cholesterol_value: 1.1) }        
        it 'returns Undesirable' do
          expect(subject.get_rating).to eq 'Undesirable'                    
        end        
      end

      context 'hdl_cholesterol_value is 1.2' do
        let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating, gender: 'female', hdl_cholesterol_value: 1.2) }        
        it 'returns Undesirable' do
          expect(subject.get_rating).to eq 'Desirable'                    
        end        
      end

      context 'hdl_cholesterol_value is 1.3' do
        let(:subject) { FactoryGirl.build(:hdl_cholesterol_rating, gender: 'female', hdl_cholesterol_value: 1.3) }        
        it 'returns Desirable' do
          expect(subject.get_rating).to eq 'Desirable'                    
        end        
      end
    end    

  end
end
