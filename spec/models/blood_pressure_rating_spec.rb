require 'rails_helper'

RSpec.describe BloodPressureRating, type: :model do
  it { expect(subject).to respond_to(:systolic) }
  it { expect(subject).to respond_to(:diastolic) }
  

  describe '#get_rating' do
    context 'systolic not set' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating) }      

      it 'raises error' do
        expect(lambda { subject.get_rating }).to raise_error(RuntimeError)
      end
    end

    context 'diastolic not set' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating) }      

      it 'raises error' do
        expect(lambda { subject.get_rating }).to raise_error(RuntimeError)
      end
    end

    context 'systolic is 119 and diastolic is 79' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 119, diastolic: 79) }      

      it 'returns Optimal Blood Pressure' do
        expect(subject.get_rating).to eq 'Optimal Blood Pressure'
      end
    end

    context 'systolic is 120 and diastolic is 80' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 120, diastolic: 80) }

      it 'returns Optimal Blood Pressure' do
        expect(subject.get_rating).to eq 'Optimal Blood Pressure'
      end
    end

    context 'systolic is 121 and diastolic is 81' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 121, diastolic: 81) }

      it 'returns Normal Blood Pressure' do
        expect(subject.get_rating).to eq 'Normal Blood Pressure'
      end
    end

    context 'systolic is 129 and diastolic is 84' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 129, diastolic: 84) }

      it 'returns Normal Blood Pressure' do
        expect(subject.get_rating).to eq 'Normal Blood Pressure'
      end
    end

    context 'systolic is 130 and diastolic is 85' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 130, diastolic: 85) }

      it 'returns Normal Blood Pressure' do
        expect(subject.get_rating).to eq 'Normal Blood Pressure'
      end
    end

    context 'systolic is 131 and diastolic is 86' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 131, diastolic: 86) }

      it 'returns High-Normal Blood Pressure' do
        expect(subject.get_rating).to eq 'High-Normal Blood Pressure'
      end
    end

    context 'systolic is 139 and diastolic is 89' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 139, diastolic: 89) }

      it 'returns High-Normal Blood Pressure' do
        expect(subject.get_rating).to eq 'High-Normal Blood Pressure'
      end
    end

    context 'systolic is 140 and diastolic is 90' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 140, diastolic: 90) }

      it 'returns Grade 1 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 1 Hypertension'
      end
    end

    context 'systolic is 141 and diastolic is 91' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 141, diastolic: 91) }

      it 'returns Grade 1 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 1 Hypertension'
      end
    end

    context 'systolic is 159 and diastolic is 99' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 159, diastolic: 99) }

      it 'returns Grade 1 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 1 Hypertension'
      end
    end

    context 'systolic is 160 and diastolic is 100' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 160, diastolic: 100) }

      it 'returns Grade 2 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 2 Hypertension'
      end
    end

    context 'systolic is 161 and diastolic is 101' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 161, diastolic: 101) }

      it 'returns Grade 2 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 2 Hypertension'
      end
    end

    context 'systolic is 179 and diastolic is 109' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 179, diastolic: 109) }

      it 'returns Grade 2 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 2 Hypertension'
      end
    end

    context 'systolic is 180 and diastolic is 110' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 180, diastolic: 110) }

      it 'returns Grade 3 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 3 Hypertension'
      end
    end

    context 'systolic is 181 and diastolic is 111' do
      let(:subject) { FactoryGirl.build(:blood_pressure_rating, systolic: 181, diastolic: 111) }

      it 'returns Grade 3 Hypertension' do
        expect(subject.get_rating).to eq 'Grade 3 Hypertension'
      end
    end    

  end
end
