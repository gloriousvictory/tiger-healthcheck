require 'rails_helper'

RSpec.describe ClientStep, type: :model do
  it { should belong_to :step }
  it { should belong_to :client }
end
