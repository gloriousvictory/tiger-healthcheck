require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many :assignments }
  it { should have_many :employee_checks }
  it { should validate_uniqueness_of :email }
end
