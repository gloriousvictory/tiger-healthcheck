require 'rails_helper'

RSpec.describe TotalCholesterolRating, type: :model do
  describe '#get_rating' do

    pending 'if total_cholesterol is blank throws exception'

    context 'total_cholesterol is 3' do
      before { subject.stub(:total_cholesterol) { 3 } }

      it 'returns Ideal' do
        expect(subject.get_rating).to eq 'Ideal'
      end     
    end
    
    context 'total_cholesterol is 4' do
      before { subject.stub(:total_cholesterol) { 4 } }

      it 'returns Desirable' do
        expect(subject.get_rating).to eq 'Desirable'
      end
    end

    context 'total_cholesterol is 5' do
      before { subject.stub(:total_cholesterol) { 5 } }
      
      it 'returns Increased Risk' do
        expect(subject.get_rating).to eq 'Increased Risk'
      end
    end

    context 'total_cholesterol is 6' do
      before { subject.stub(:total_cholesterol) { 6 } }

      it 'returns Increased Risk' do
        expect(subject.get_rating).to eq 'Increased Risk'
      end
    end

    context 'total_cholesterol is 7' do
      before { subject.stub(:total_cholesterol) { 7 } }

      it 'returns Undesirable' do
        expect(subject.get_rating).to eq 'Undesirable'
      end
    end    
  end
end
