require 'rails_helper'

RSpec.describe BodyFatRating, type: :model do
  describe '#get_rating' do
    context 'is male' do
      before do
        @body_fat_rating = FactoryGirl.build(:body_fat_rating, :male)
      end

      it 'should call get_rating_male' do
        expect(@body_fat_rating).to receive(:get_rating_male)
        @body_fat_rating.get_rating
      end

      it 'should not call get_rating_female' do
        expect(@body_fat_rating).not_to receive(:get_rating_female)
        @body_fat_rating.get_rating        
      end
    end

    context 'is female' do
      before do
        @body_fat_rating = FactoryGirl.build(:body_fat_rating, :female)
      end

      it 'should call get_rating_female' do
        expect(@body_fat_rating).to receive(:get_rating_female)
        @body_fat_rating.get_rating
      end

      it 'should not call get_rating_male' do
        expect(@body_fat_rating).not_to receive(:get_rating_male)
        @body_fat_rating.get_rating
      end      
    end
  end

  # Male
  describe '#get_rating_male' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :male)
    end    

    context 'age is 19' do
      before do
        @body_fat_rating.stub(:age) { 19 }        
      end

      it 'should not call get_rating_male_cat_1' do
        expect(@body_fat_rating).not_to receive(:get_rating_male_cat_1)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 20' do
      before do
        @body_fat_rating.stub(:age) { 20 }        
      end

      it 'should call get_rating_male_cat_1' do
        expect(@body_fat_rating).to receive(:get_rating_male_cat_1)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 39' do
      before do
        @body_fat_rating.stub(:age) { 39 }        
      end

      it 'should call get_rating_male_cat_1' do
        expect(@body_fat_rating).to receive(:get_rating_male_cat_1)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 40' do
      before do
        @body_fat_rating.stub(:age) { 40 }        
      end

      it 'should call get_rating_male_cat_2' do
        expect(@body_fat_rating).to receive(:get_rating_male_cat_2)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 59' do
      before do
        @body_fat_rating.stub(:age) { 59 }        
      end

      it 'should call get_rating_male_cat_2' do
        expect(@body_fat_rating).to receive(:get_rating_male_cat_2)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 60' do
      before do
        @body_fat_rating.stub(:age) { 60 }        
      end

      it 'should call get_rating_male_cat_3' do
        expect(@body_fat_rating).to receive(:get_rating_male_cat_3)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 79' do
       before do
        @body_fat_rating.stub(:age) { 79 }        
      end

      it 'should call get_rating_male_cat_3' do
        expect(@body_fat_rating).to receive(:get_rating_male_cat_3)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 80' do
      before do
        @body_fat_rating.stub(:age) { 80 }        
      end

      it 'should not call get_rating_male_cat_3' do
        expect(@body_fat_rating).not_to receive(:get_rating_male_cat_3)
        @body_fat_rating.get_rating        
      end
    end
  end

  # Female
  describe '#get_rating_female' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :female)
    end    

    context 'age is 19' do
      before do
        @body_fat_rating.stub(:age) { 19 }        
      end

      it 'should not call get_rating_female_cat_1' do
        expect(@body_fat_rating).not_to receive(:get_rating_female_cat_1)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 20' do
      before do
        @body_fat_rating.stub(:age) { 20 }        
      end

      it 'should call get_rating_female_cat_1' do
        expect(@body_fat_rating).to receive(:get_rating_female_cat_1)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 39' do
      before do
        @body_fat_rating.stub(:age) { 39 }        
      end

      it 'should call get_rating_female_cat_1' do
        expect(@body_fat_rating).to receive(:get_rating_female_cat_1)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 40' do
      before do
        @body_fat_rating.stub(:age) { 40 }        
      end

      it 'should call get_rating_female_cat_2' do
        expect(@body_fat_rating).to receive(:get_rating_female_cat_2)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 59' do
      before do
        @body_fat_rating.stub(:age) { 59 }        
      end

      it 'should call get_rating_female_cat_2' do
        expect(@body_fat_rating).to receive(:get_rating_female_cat_2)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 60' do
      before do
        @body_fat_rating.stub(:age) { 60 }        
      end

      it 'should call get_rating_female_cat_3' do
        expect(@body_fat_rating).to receive(:get_rating_female_cat_3)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 79' do
       before do
        @body_fat_rating.stub(:age) { 79 }        
      end

      it 'should call get_rating_female_cat_3' do
        expect(@body_fat_rating).to receive(:get_rating_female_cat_3)
        @body_fat_rating.get_rating        
      end
    end

    context 'age is 80' do
      before do
        @body_fat_rating.stub(:age) { 80 }        
      end

      it 'should not call get_rating_female_cat_3' do
        expect(@body_fat_rating).not_to receive(:get_rating_female_cat_3)
        @body_fat_rating.get_rating        
      end
    end    
  end

  # Male aged 20 to 39
  describe '#get_rating_male_cat_1' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :male)
    end

    context 'total_body_fat is 6' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 6 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Under')
      end
    end

    context 'total_body_fat is 7' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 7 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Under')
      end
    end

    context 'total_body_fat is 8' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 8 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Healthy')
      end
    end    

    context 'total_body_fat is 19' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 19 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Healthy')
      end
    end

    context 'total_body_fat is 20' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 20 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Overfat')
      end
    end

    context 'total_body_fat is 25' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 25 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Overfat')
      end
    end

    context 'total_body_fat is 26' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 26 }        
      end

      it 'should return Obese' do
         expect(@body_fat_rating.get_rating_male_cat_1).to eq('Obese')
      end
    end
  end

  # Male aged 40 to 59
  describe '#get_rating_male_cat_2' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :male)
    end

    context 'total_body_fat is 9' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 9 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Under')
      end
    end

    context 'total_body_fat is 10' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 10 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Under')
      end
    end

    context 'total_body_fat is 11' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 11 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Healthy')
      end
    end    

    context 'total_body_fat is 22' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 22 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Healthy')
      end
    end

    context 'total_body_fat is 23' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 23 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Overfat')
      end
    end

    context 'total_body_fat is 28' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 28 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Overfat')
      end
    end

    context 'total_body_fat is 29' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 29 }        
      end

      it 'should return Obese' do
         expect(@body_fat_rating.get_rating_male_cat_2).to eq('Obese')
      end
    end    
  end

  # 60 to 79
  describe '#get_rating_male_cat_3' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :male)
    end

    context 'total_body_fat is 11' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 11 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Under')
      end
    end

    context 'total_body_fat is 12' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 12 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Under')
      end
    end

    context 'total_body_fat is 13' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 13 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Healthy')
      end
    end    

    context 'total_body_fat is 25' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 25 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Healthy')
      end
    end

    context 'total_body_fat is 26' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 26 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Overfat')
      end
    end

    context 'total_body_fat is 30' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 30 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Overfat')
      end
    end

    context 'total_body_fat is 31' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 31 }        
      end

      it 'should return Obese' do
         expect(@body_fat_rating.get_rating_male_cat_3).to eq('Obese')
      end
    end    
  end


  # Female aged 20 to 39
  describe '#get_rating_female_cat_1' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :female)
    end

    context 'total_body_fat is 20' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 20 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Under')
      end
    end

    context 'total_body_fat is 21' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 21 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Under')
      end
    end

    context 'total_body_fat is 22' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 22 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Healthy')
      end
    end    

    context 'total_body_fat is 33' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 33 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Healthy')
      end
    end

    context 'total_body_fat is 34' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 34 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Overfat')
      end
    end

    context 'total_body_fat is 39' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 39 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Overfat')
      end
    end

    context 'total_body_fat is 40' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 40 }        
      end

      it 'should return Obese' do
         expect(@body_fat_rating.get_rating_female_cat_1).to eq('Obese')
      end
    end
  end

  # Female aged 40 to 59
  describe '#get_rating_female_cat_2' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :female)
    end

    context 'total_body_fat is 22' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 22 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Under')
      end
    end

    context 'total_body_fat is 23' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 23 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Under')
      end
    end

    context 'total_body_fat is 24' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 24 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Healthy')
      end
    end    

    context 'total_body_fat is 34' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 34 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Healthy')
      end
    end

    context 'total_body_fat is 35' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 35 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Overfat')
      end
    end

    context 'total_body_fat is 40' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 40 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Overfat')
      end
    end

    context 'total_body_fat is 41' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 41 }        
      end

      it 'should return Obese' do
         expect(@body_fat_rating.get_rating_female_cat_2).to eq('Obese')
      end
    end    
  end

  # Female 60 to 79
  describe '#get_rating_female_cat_3' do
    before do
      @body_fat_rating = FactoryGirl.build(:body_fat_rating, :female)
    end

    context 'total_body_fat is 23' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 23 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Under')
      end
    end

    context 'total_body_fat is 24' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 24 }        
      end

      it 'should return Under' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Under')
      end
    end

    context 'total_body_fat is 34' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 34 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Healthy')
      end
    end    

    context 'total_body_fat is 35' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 35 }        
      end

      it 'should return Healthy' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Healthy')
      end
    end

    context 'total_body_fat is 36' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 36 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Overfat')
      end
    end

    context 'total_body_fat is 42' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 42 }        
      end

      it 'should return Overfat' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Overfat')
      end
    end

    context 'total_body_fat is 43' do
      before do
        @body_fat_rating.stub(:total_body_fat) { 43 }        
      end

      it 'should return Obese' do
         expect(@body_fat_rating.get_rating_female_cat_3).to eq('Obese')
      end
    end    
  end

end
