require 'rails_helper'

RSpec.describe Client, type: :model do
  it { should validate_presence_of :name }
  it { should validate_uniqueness_of :name }

  it { should have_many :users }
  it { should have_many :departments }
  it { should have_many :steps }

  describe '#get_steps' do
    pending
  end
end
