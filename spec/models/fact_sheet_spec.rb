require 'rails_helper'

RSpec.describe FactSheet, type: :model do
  it { should belong_to :fact_sheet_category }
  it { should validate_presence_of :title }
  it { should validate_presence_of :pdf_file }
end
