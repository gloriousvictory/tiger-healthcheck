require 'rails_helper'

RSpec.describe FactSheetCategory, type: :model do
  it { should have_many :fact_sheets }
  it { should validate_presence_of :title }
end
