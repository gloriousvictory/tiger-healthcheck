require 'rails_helper'

RSpec.describe FactSheetRequest, type: :model do
  it { should belong_to :fact_sheet }
  it { should belong_to :employee_check }

  it { should validate_presence_of :fact_sheet }
  it { should validate_presence_of :employee_check }
end
