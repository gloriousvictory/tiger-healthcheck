require 'rails_helper'

RSpec.describe RestingHeartRateRating, type: :model do
  describe '#get_rating' do
    pending 'resting heart rate is blank throw exception'

    context 'resting heart rate is 54' do
      before { subject.stub(:resting_hr) { 54 } }

      it 'should return Excellent' do
        expect(subject.get_rating).to eq 'Excellent'
      end   
      
    end

    context 'resting heart rate is 55' do
      before { subject.stub(:resting_hr) { 55 } }

      it 'should return Excellent' do
        expect(subject.get_rating).to eq 'Excellent'
      end   
      
    end

    context 'resting heart rate is 56' do
      before { subject.stub(:resting_hr) { 56 } }
      
      it 'should return Good' do
        expect(subject.get_rating).to eq 'Good'
      end   
      
    end

    context 'resting heart rate is 69' do
      before { subject.stub(:resting_hr) { 69 } }   
      
      it 'should return Good' do
        expect(subject.get_rating).to eq 'Good'
      end 
    end

    context 'resting heart rate is 70' do
      before { subject.stub(:resting_hr) { 70 } }   
      
      it 'should return Average' do
        expect(subject.get_rating).to eq 'Average'
      end 
    end

    context 'resting heart rate is 84' do
      before { subject.stub(:resting_hr) { 84 } }   
      
      it 'should return Average' do
        expect(subject.get_rating).to eq 'Average'
      end 
    end

    context 'resting heart rate is 85' do
      before { subject.stub(:resting_hr) { 85 } }   
      
      it 'should return Poor' do
        expect(subject.get_rating).to eq 'Poor'
      end 
    end
    
    context 'resting heart rate is 86' do
      before { subject.stub(:resting_hr) { 86 } }   
      
      it 'should return Poor' do
        expect(subject.get_rating).to eq 'Poor'
      end 
    end
  end
end
