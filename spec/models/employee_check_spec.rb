require 'rails_helper'

RSpec.describe EmployeeCheck, type: :model do
  it { should belong_to :user }

  context 'on create' do
    it { should validate_presence_of :first_name }
    it { should validate_presence_of :last_name }
    it { should validate_presence_of :gender }
    it { should validate_presence_of :date_of_birth }
    it { should validate_presence_of :email }
  end

  context 'when current_step is basic' do
    before { subject.stub(:is_step_basic?) { true } }    

    it { should validate_presence_of :ethnic_code }
    it { should validate_presence_of :department }
    it { should validate_presence_of :full_part_time }
    # it { should validate_presence_of :smoker }
    # needs to validate inclusion of
    # it { should validate_presence_of :past_assessment }
    # needs to validate inclusion of
  end

  context 'when current_step is body_composition' do
    before { subject.stub(:is_step_body_composition?) { true } }   

    # it { should validate_presence_of :body_fat }
    # it { should validate_presence_of :total_water }
    it { should validate_presence_of :metric_imperial }

    context 'when metric' do
      before { subject.stub(:metric_imperial) { 'metric' } }

      it { should validate_presence_of :height_cm }
      it { should validate_presence_of :weight_kg }
    end

    context 'when imperial' do
      before { subject.stub(:metric_imperial) { 'imperial' } }
      
      it { should validate_presence_of :height_feet }
      it { should validate_presence_of :height_inches }

      it { should validate_presence_of :weight_stone }
      it { should validate_presence_of :weight_lbs }      
    end
  end

  context 'when current_step is blood_pressure' do
    before { subject.stub(:is_step_blood_pressure?) { true } }   
    
    it { should validate_presence_of :systolic }
    it { should validate_presence_of :diastolic }
    it { should validate_presence_of :resting_hr }   
  end

  context 'when current_step is cholesterol' do  
    before { subject.stub(:is_step_cholesterol?) { true } }   

    # it { should validate_presence_of :total_cholesterol }
  end

  context 'when current_step is blood_sugar' do  
    before { subject.stub(:is_step_blood_sugar?) { true } }   

    it { should validate_presence_of :test_type }
    it { should validate_presence_of :blood_sugar }
  end

  describe '#get_bmi' do
    context 'is metric' do
      before do
        @employee_check = FactoryGirl.build(:employee_check, :metric)
      end
      it 'calls get_bmi_metric' do
        expect(@employee_check).to receive(:get_bmi_metric)

        @employee_check.get_bmi        
      end
    end

    context 'is imperial' do
      before do
        @employee_check = FactoryGirl.build(:employee_check, :imperial)
      end

      it 'calls get_bmi_imperial' do
        expect(@employee_check).to receive(:get_bmi_imperial)

        @employee_check.get_bmi        
      end
    end
  end

  describe '#get_bmi_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check, :metric)
    end

    it 'calls BmiRating.get_rating' do
      expect_any_instance_of(BmiRating).to receive(:get_rating)

      @employee_check.get_bmi_rating     
    end
  end

  describe '#get_bmi_imperial' do
    context 'height is 5ft10 and weight is 14st2' do
      before do
        @employee_check = FactoryGirl.build(:employee_check, :imperial)
      end
      
      it 'should return 28.41' do
        expect(@employee_check.get_bmi).to eq(28.41)
      end
    end
  end

  describe '#get_bmi_metric' do
    context 'height is 178cm and weight is 90kg' do
      before do
        @employee_check = FactoryGirl.build(:employee_check, :metric)
      end
      
      it 'should return 28.41' do
        expect(@employee_check.get_bmi).to eq(28.41)
      end
    end    
  end

  describe '#get_resting_heart_rate_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check, :metric)
    end
    
    it 'calls RestingHeartRateRating.get_rating' do
      expect_any_instance_of(RestingHeartRateRating).to receive(:get_rating)

      @employee_check.get_resting_heart_rate_rating     
    end  
  end

  pending '#get_age'

  describe '#get_total_cholesterol_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check, :metric)
    end
    
    it 'calls RestingHeartRateRating.get_rating' do
      expect_any_instance_of(TotalCholesterolRating).to receive(:get_rating)

      @employee_check.get_total_cholesterol_rating     
    end    
  end
  
  describe '#get_total_cholesterol_hdl_ratio' do
    before do
        @employee_check = FactoryGirl.build(:employee_check)

    end
    
    it 'calls TotalCholesterolHdlRatioRating.get_rating' do
      expect_any_instance_of(TotalCholesterolHdlRatioRating).to receive(:get_rating)

      @employee_check.get_total_cholesterol_hdl_ratio_rating     
    end    
  end

  describe '#get_total_body_fat_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check)
    end
    
    it 'calls BodyFatRating.get_rating' do
      expect_any_instance_of(BodyFatRating).to receive(:get_rating)

      @employee_check.get_total_body_fat_rating     
    end 
  end

  describe '#get_hdl_cholesterol_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check)
    end
    
    it 'calls HdlCholesterolRating.get_rating' do
      expect_any_instance_of(HdlCholesterolRating).to receive(:get_rating)

      @employee_check.get_hdl_cholesterol_rating     
    end 
  end

  describe '#get_total_cholesterol_hdl_ratio_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check)
    end
    
    it 'calls TotalCholesterolHdlRatioRating.get_rating' do
      expect_any_instance_of(TotalCholesterolHdlRatioRating).to receive(:get_rating)

      @employee_check.get_total_cholesterol_hdl_ratio_rating     
    end 
  end

  describe '#get_blood_pressure_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check)
    end
    
    it 'calls TotalCholesterolHdlRatioRating.get_rating' do
      expect_any_instance_of(BloodPressureRating).to receive(:get_rating)

      @employee_check.get_blood_pressure_rating     
    end 
  end

  describe '#get_blood_sugar_rating' do
    before do
        @employee_check = FactoryGirl.build(:employee_check)
    end
    
    it 'calls BloodSugarRating.get_rating' do
      expect_any_instance_of(BloodSugarRating).to receive(:get_rating)

      @employee_check.get_blood_sugar_rating     
    end 
  end

end
