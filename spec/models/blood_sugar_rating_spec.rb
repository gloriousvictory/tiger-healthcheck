require 'rails_helper'

RSpec.describe BloodSugarRating, type: :model do
  it { expect(subject).to respond_to(:blood_sugar) }  

  describe '#get_rating' do
    context 'blood_sugar not set' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating) }      

      it 'raises error' do
        expect(lambda { subject.get_rating }).to raise_error(RuntimeError)
      end
    end

    context 'blood_sugar is 3.9' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 3.9) }      

      it 'returns Undesirable' do
        expect(subject.get_rating).to eq 'Undesirable'
      end
    end

    context 'blood_sugar is 4' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 4) }      

      it 'returns Desirable' do
        expect(subject.get_rating).to eq 'Desirable'
      end
    end

    context 'blood_sugar is 4.1' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 4.1) }      

      it 'returns Desirable' do
        expect(subject.get_rating).to eq 'Desirable'
      end
    end

    context 'blood_sugar is 7.9' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 7.9) }      

      it 'returns Desirable' do
        expect(subject.get_rating).to eq 'Desirable'
      end
    end

    context 'blood_sugar is 8' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 8) }      

      it 'returns Increased Risk' do
        expect(subject.get_rating).to eq 'Increased Risk'
      end
    end    

    context 'blood_sugar is 8.1' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 8.1) }      

      it 'returns Increased Risk' do
        expect(subject.get_rating).to eq 'Increased Risk'
      end
    end

    context 'blood_sugar is 10.9' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 10.9) }      

      it 'returns Increased Risk' do
        expect(subject.get_rating).to eq 'Increased Risk'
      end
    end  

    context 'blood_sugar is 11' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 11) }      

      it 'returns Undesirable' do
        expect(subject.get_rating).to eq 'Undesirable'
      end
    end 

    context 'blood_sugar is 11.1' do
      let(:subject) { FactoryGirl.build(:blood_sugar_rating, blood_sugar: 11.1) }      

      it 'returns Undesirable' do
        expect(subject.get_rating).to eq 'Undesirable'
      end
    end    

  end
end
