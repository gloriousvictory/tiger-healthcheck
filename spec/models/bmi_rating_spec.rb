require 'rails_helper'

RSpec.describe BmiRating, type: :model do
  describe '#get_rating' do
    pending 'bmi not set throws exception'

    context 'bmi is 18.4' do
      before { subject.stub(:bmi) { 18.4 } }

      it 'should return Underweight' do
        expect(subject.get_rating).to eq 'Underweight'
      end   
    end

    context 'bmi is 18.5' do
      before { subject.stub(:bmi) { 18.5 } }

      it 'should return Normal Range' do
        expect(subject.get_rating).to eq 'Underweight'
      end
    end

    context 'bmi is 18.6' do
      before { subject.stub(:bmi) { 18.6 } }

      it 'should return Normal Range' do
        expect(subject.get_rating).to eq 'Normal Range'
      end
    end    

    context 'bmi is 24.99' do
      before { subject.stub(:bmi) { 24.99 } } 

      it 'should return Normal Range' do
        expect(subject.get_rating).to eq 'Normal Range'      
      end
    end
     
    context 'bmi is 25' do
      before { subject.stub(:bmi) { 25 } }   

      it 'should return Overweight' do
        expect(subject.get_rating).to eq 'Overweight'        
      end
    end

    context 'bmi is 29.99' do
      before { subject.stub(:bmi) { 29.99 } }   

      it 'should return Overweight' do
        expect(subject.get_rating).to eq 'Overweight'        
      end
    end

    context 'bmi is 30' do
      before { subject.stub(:bmi) { 30 } }   

      it 'should return Obese Class 1' do
        expect(subject.get_rating).to eq 'Obese Class 1'        
      end
    end

    context 'bmi is 34.99' do
      before { subject.stub(:bmi) { 34.99 } }   

      it 'should return Obese Class 1' do
        expect(subject.get_rating).to eq 'Obese Class 1'        
      end
    end 

    context 'bmi is 35' do
      before { subject.stub(:bmi) { 35 } }   

      it 'should return Obese Class 2' do
        expect(subject.get_rating).to eq 'Obese Class 2'        
      end
    end

    context 'bmi is 39.99' do
      before { subject.stub(:bmi) { 39.99 } }   

      it 'should return Obese Class 2' do
        expect(subject.get_rating).to eq 'Obese Class 2'        
      end
    end

    context 'bmi is 40' do
      before { subject.stub(:bmi) { 40 } }   

      it 'should return Obese Class 3' do
        expect(subject.get_rating).to eq 'Obese Class 3'        
      end
    end

    context 'bmi is 41' do
      before { subject.stub(:bmi) { 41 } }   

      it 'should return Obese Class 3' do
        expect(subject.get_rating).to eq 'Obese Class 3'        
      end
    end    
  end
end
