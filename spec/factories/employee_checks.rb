FactoryGirl.define do
  factory :employee_check do
    total_cholesterol 10
    hdl_cholesterol 5
    date_of_birth '01/01/2000'

    trait :metric do
      metric_imperial 'metric' 
      height_cm 178
      weight_kg 90
    end

    trait :imperial do
      metric_imperial 'imperial'
      height_feet 5
      height_inches 10
      weight_stone 14
      weight_lbs 2
    end
  end

end
