FactoryGirl.define do
  factory :body_fat_rating do
    age 20
    total_body_fat 20

    trait :male do
      gender 'male'
    end

    trait :female do
      gender 'female'
    end
  end

end
